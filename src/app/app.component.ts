import { Component, OnInit, ViewChild } from '@angular/core';
import { fabric } from 'fabric';
import { MatMenuTrigger } from '@angular/material';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    canvas: any;
    x: number;
    y: number;
    menuDisplayed = false;
    objectMenuDisplayed = false;

    ngOnInit() {
        this.canvas = new fabric.Canvas('myCanvas');
        this.loadBackgroundImage();

        this.canvas.on('mouse:down', (event) => {
            if (this.menuDisplayed) {
                const menu = document.getElementById('menu');
                menu.style.display = 'none';
            }
        })

        this.canvas.on('mouse:down:before', (event) => {
            [this.x, this.y] = [event.pointer.x, event.pointer.y];
            this.menuDisplayed = true;
            const menu = document.getElementById('menu');
            menu.style.display = 'block';
            menu.style.position = 'absolute';
            menu.style.top = `${this.y + 5}px`;
            menu.style.left = `${this.x - 10}px`;
        });
    }

    loadBackgroundImage() {
        this.canvas.setBackgroundImage('assets/welding2.jpg', this.canvas.renderAll.bind(this.canvas), {
            backgroundImageOpacity: 1,
            backgroundImageStretch: false
        });
    }

    clearCanvas() {
        this.canvas.clear();
        this.loadBackgroundImage();
    }

    saveLayer() {
        localStorage.setItem('obj', JSON.stringify(this.canvas.toJSON()));
    }

    loadLayer() {
        const elements = JSON.parse(localStorage.getItem('obj'));
        this.canvas.loadFromJSON(elements, () => {
            this.canvas.renderAll();
        });
    }

    addArrow() {
        fabric.loadSVGFromURL('assets/arrow.svg', (objects, options) => {
            const obj = fabric.util.groupSVGElements(objects, {
                left: this.x,
                top: this.y
            });
            obj.on('mousedblclick', (event) => {
                [this.x, this.y] = [event.pointer.x, event.pointer.y];
                const menu = document.getElementById('deleteMenu');
                menu.style.display = 'block';
                menu.style.position = 'absolute';
                menu.style.top = `${this.y + 5}px`;
                menu.style.left = `${this.x - 10}px`;
            })
            this.canvas.add(obj).renderAll();
        });
    }

    addText() {
        const text = new fabric.IText('Tap and Type', {
            left: this.x,
            top: this.y,
            fill: 'black'
        });

        text.on('mousedblclick', (event) => {
            [this.x, this.y] = [event.pointer.x, event.pointer.y];
            const menu = document.getElementById('deleteMenu');
            menu.style.display = 'block';
            menu.style.position = 'absolute';
            menu.style.top = `${this.y + 5}px`;
            menu.style.left = `${this.x - 10}px`;
        })
        this.canvas.add(text);
    }

    removeObject() {
        this.canvas.getActiveObjects().forEach((obj) => {
            this.canvas.remove(obj);
        });
        this.canvas.discardActiveObject().renderAll();
        const menu = document.getElementById('deleteMenu');
        menu.style.display = 'none';
    }
}
